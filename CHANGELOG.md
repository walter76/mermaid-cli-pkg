# Release Notes

## Release v0.0.1

* initial release
* based on mermaid-cli v8.11.5
* uses node.exe v14.5.0

### v0.0.1 Assets

* Package: [mermaid-cli-8.11.5-pkg-0.0.1.7z](https://my.hidrive.com/lnk/gLAjFRn4)
* [Direct Download Link](https://my.hidrive.com/api/sharelink/download?id=gLAjFRn4)

## Release v0.0.2

* added puppeteer-config.json to disable the sandbox in puppeteer
* based on mermaid-cli v8.11.5
* uses node.exe v14.5.0

### v0.0.2 Assets

* Package: [mermaid-cli-8.11.5-pkg-0.0.2.7z](https://my.hidrive.com/lnk/Q3gDlzNI)
* [Direct Download Link](https://my.hidrive.com/api/sharelink/download?id=Q3gDlzNI)

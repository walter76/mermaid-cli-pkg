# mermaid-cli-pkg

The only purpose of this repository is to create a redistributable self-contained
package of the mermaid-cli. It pulls the version of [mermaid-cli](https://github.com/mermaid-js/mermaid-cli)
and [nodejs](https://nodejs.org/en/) [executable](https://nodejs.org/dist/latest-v14.x/win-x64/)
to put it into a zip file.

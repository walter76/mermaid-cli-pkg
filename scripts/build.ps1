param(
  [Parameter(Mandatory=$false)][string]$Src = ".\src",
  [Parameter(Mandatory=$false)][string]$Dist = ".\dist"
)

Set-StrictMode -Version Latest
$ErrorActionPreference = 'Stop'

function Get-7Zip() {
  $sevenZip = "7z.exe"
  if(Get-Command $sevenZip -ErrorAction SilentlyContinue) {
    return $sevenZip
  }
  
  $sevenZip = "C:\Program Files\7-Zip\7z.exe"
  if(Get-Command $sevenZip -ErrorAction SilentlyContinue) {
    return $sevenZip
  }
  
  throw "7z.exe not found!"
}

function New-Directory ($Path) {
  Remove-Item $Path -Recurse -ErrorAction SilentlyContinue
  New-Item $Path -ItemType directory | Out-Null
  return $Path
}
  
$scriptPath = Split-Path -parent $MyInvocation.MyCommand.Path

Write-Output "Detecting 7zip executable..."
$sevenZip = Get-7Zip

Write-Output "Creating temporary directories..."
$tempPath = New-Directory "$scriptPath\..\temp"
Write-Output "Using temporary directory $tempPath"

Write-Output "Copying files to temporary directory..."
Copy-Item -Recurse "$src\node_modules" $tempPath
Copy-Item "$src\package.json" $tempPath
Copy-Item "$src\puppeteer-config.json" $tempPath
Copy-Item "$src\yarn.lock" $tempPath

$mermaidCliVersion = "8.11.5"
$version = "0.0.2"
$packageFilename = "mermaid-cli-$mermaidCliVersion-pkg-$version.7z"
$packagePath = "..\$Dist\$packageFilename"

Write-Output "Create package $packagePath..."
Set-Location -Path $tempPath
& $sevenZip a $packagePath *
